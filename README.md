# TTPC(Tokyo Tech Programming Contest) 2015
東京工業大学プログラミングコンテスト2015は以下のメンバーで運営されました

- camypaper
- piroz
- shimomire
- tokoharu
- yosupo


# 解説スライド & データセット #
* A : ぼくの学生証
* B : ラー油 [解説](https://bitbucket.org/ttpc2014/ttpc2015/downloads/B.pdf) / [データセット](https://bitbucket.org/ttpc2014/ttpc2015/downloads/B.zip)
* C : おおおかやま [解説](https://bitbucket.org/ttpc2014/ttpc2015/downloads/C.pdf) / [データセット](https://bitbucket.org/ttpc2014/ttpc2015/downloads/C.zip)
* D : 文字列と素数 [解説](https://bitbucket.org/ttpc2014/ttpc2015/downloads/D.pdf)
* E : マス目色ぬり [解説](https://bitbucket.org/ttpc2014/ttpc2015/downloads/マス目色ぬり.pdf)
* F : レシート [解説](https://bitbucket.org/ttpc2014/ttpc2015/downloads/F.pdf) / [データセット](https://bitbucket.org/ttpc2014/ttpc2015/downloads/F.zip)
* G : titech分離 [解説](https://bitbucket.org/ttpc2014/ttpc2015/downloads/G.pdf) / [データセット](https://bitbucket.org/ttpc2014/ttpc2015/downloads/G.zip)
* H : 包囲 [解説](https://bitbucket.org/ttpc2014/ttpc2015/downloads/H.pdf) / [データセット](https://bitbucket.org/ttpc2014/ttpc2015/downloads/H.zip)
* I : そーっとソート [解説](https://bitbucket.org/ttpc2014/ttpc2015/downloads/そーっとソート.pdf)
* J : 指さし [解説](https://bitbucket.org/ttpc2014/ttpc2015/downloads/J.pdf) / [データセット](https://bitbucket.org/ttpc2014/ttpc2015/downloads/J.zip)
* K : 麻雀
* L : グラフ色ぬり [解説](https://bitbucket.org/ttpc2014/ttpc2015/downloads/グラフ色ぬり.pdf)
* M : コインと無向グラフ [解説](https://bitbucket.org/ttpc2014/ttpc2015/downloads/M.pdf)
* N : 何かグラフの問題
* O : 数列色ぬり [解説](https://bitbucket.org/ttpc2014/ttpc2015/downloads/数列色ぬり.pdf)
* P : Dancing stars on regular expression! [解説](https://bitbucket.org/ttpc2014/ttpc2015/downloads/P.pdf)